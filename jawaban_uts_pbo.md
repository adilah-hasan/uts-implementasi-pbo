# No 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)

menampilkan pseudo code dari algoritma yang menyelesaikan class total transfer pada projek flipcloneapps : 

`START
    CONNECT_TO_DATABASE
    
    QUERY = "SELECT jumlah_transfer FROM tabel_transfer"
    RESULT = EXECUTE_QUERY(QUERY)
    
    totalTransfer = 0 // Variabel baru untuk menampung total transfer
    
    IF (RESULT has rows)
        FOREACH row IN RESULT
            jumlahTransfer = row["jumlah_transfer"]
            totalTransfer += jumlahTransfer
        END FOREACH
    END IF
    
    // Menampilkan total hasil transfer
    PRINT "Total hasil transfer: " + totalTransfer
    
    // Menutup koneksi
    CLOSE_DATABASE_CONNECTION
END
`

source codenya : 

`<?php
// Menghubungkan ke database
$host = "localhost";
$username = "username";
$password = "password";
$dbname = "transfer";

$connection = new mysqli($host, $username, $password, $dbname);

// Memeriksa koneksi
if ($connection->connect_error) {
    die("Koneksi gagal: " . $connection->connect_error);
}

// Mengambil data dari database
$sql = "SELECT jumlah_transfer FROM tabel_transfer";
$result = $connection->query($sql);

$totalTransfer = 0; // Variabel baru untuk menampung total transfer

if ($result->num_rows > 0) {
    // Looping melalui setiap baris data
    foreach ($result as $row) {
        $jumlahTransfer = $row["jumlah_transfer"];
        $totalTransfer += $jumlahTransfer;
    }
}

// Menampilkan total hasil transfer
echo "Total hasil transfer: " . $totalTransfer;

// Menutup koneksi
$connection->close();
?>`

algoritma diatas merupakan algoritma dari pendekatan matematika yang menjumlahkan total keseluruhan transfer yang telah dilakukan oleh user.

# No 2
Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

algoritma diatas merupakan algoritma dari pendekatan matematika yang menjumlahkan total keseluruhan transfer yang telah dilakukan oleh user.

# No 3
Mampu menjelaskan konsep dasar OOP

OOP (Object-Oriented Programming) adalah paradigma pemrograman yang berfokus pada pemodelan sistem komputer menggunakan objek-objek yang memiliki atribut dan perilaku tertentu. Konsep OOP berfokus pada pengorganisasian kode program ke dalam objek-objek yang dapat berinteraksi satu sama lain.

1. Enkapsulasi (Encapsulation): Enkapsulasi adalah konsep dalam OOP yang menggabungkan data dan metode-metode yang beroperasi pada data tersebut ke dalam satu unit yang disebut objek. Objek menyembunyikan implementasi dan rincian internalnya dari dunia luar, dan hanya menyediakan antarmuka yang jelas untuk berinteraksi dengan objek tersebut.

2. Pewarisan (Inheritance): Pewarisan memungkinkan pembuatan kelas baru berdasarkan kelas yang sudah ada. Kelas yang baru dibuat disebut sebagai kelas turunan atau subclass, sementara kelas yang digunakan sebagai dasar untuk membuat kelas turunan disebut kelas induk atau superclass. Dengan pewarisan, kelas turunan dapat mewarisi atribut dan metode dari kelas induknya, sehingga memungkinkan untuk memperluas dan mengubah perilaku kelas yang sudah ada.

3. Polimorfisme (Polymorphism): Polimorfisme adalah kemampuan suatu objek untuk mengambil bentuk atau perilaku yang berbeda dalam konteks yang berbeda. Dalam OOP, polimorfisme dapat dicapai melalui konsep overriding (mengganti metode yang ada) dan overloading (membuat metode dengan nama yang sama tapi dengan parameter yang berbeda). Polimorfisme memungkinkan untuk memperlakukan objek dengan cara yang seragam, terlepas dari kelas spesifik objek tersebut.

4. Abstraksi (Abstraction): Abstraksi melibatkan penyederhanaan kompleksitas dengan memodelkan objek dalam sistem komputer. Abstraksi memungkinkan untuk mengidentifikasi atribut dan perilaku yang penting dari suatu objek dan mengabaikan detail yang tidak relevan. Dalam OOP, abstraksi dapat dicapai melalui pembuatan kelas abstrak yang hanya menyediakan kerangka dasar dan metode-metode yang harus diimplementasikan oleh kelas turunannya.



# No 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

penggunaan enkapsulasi yang saya gunakan pada project saya yaitu dengan mendeklarasikan variabel dengan syntax public, privat, maupun protected sesuai dengan kebutuhan pada projek saya.

`<?php

class db_registrasi
{
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "user";
    private $koneksi = "";

    function __construct()
    {
        $this->koneksi = mysqli_connect($this->host, $this->username, $this->password, $this->database);
        if (mysqli_connect_errno()) {
            echo "Koneksi database gagal : " . mysqli_connect_error();
        }
    }

    function getData()
    {
        $hasil = [];
        $data = mysqli_query($this->koneksi, "select * from user");
        while ($row = mysqli_fetch_array($data)) {
            $hasil[] = $row;
        }

        return $hasil;
    }

    function tambah_data($username, $email, $password, $bank, $noRek)
    {
        mysqli_query($this->koneksi, "insert into user (username, email, password, bank, noRek) values ('$username', '$email' , '$password', '$bank', '$noRek' )");
    }

    function get_data_byid($id)
    {
        // $data = mysqli_query($this->koneksi, "select * from user where id='$id'");
        return mysqli_query($this->koneksi, "select * from user where id='$id'")->fetch_array();
    }

    function edit_data($username, $email, $password, $id, $bank, $noRek)
    {
        mysqli_query($this->koneksi, "update user set username='$username', email='$email', password='$password', where id='$id', bank='$bank', noRek ='$noRek'");
    }

    function hapus_data($id)
    {
        mysqli_query($this->koneksi, "delete from user where id='$id'");
    }
}
?>`


# No 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)

`<?php

abstract class Ewallet {
    protected $saldo;

    public function __construct($jumlahSaldo) {
        $this->saldo = $jumlahSaldo;
    }

    abstract public function topUp($jumlah);

    public function getSaldo() {
        return $this->saldo;
    }
}


class GoPay extends Ewallet {
    public function topUp($jumlah) {
        echo "Top-up GoPay: " . $jumlah . PHP_EOL;
        $this->saldo += $jumlah;
    }
}


class OVO extends Ewallet {
    public function topUp($jumlah) {
        echo "Top-up OVO: " . $jumlah . PHP_EOL;
        $this->saldo += $jumlah;
    }
}`

# No 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

pada perinsipnya, deifinsi polimorfisme adalah kemampuan suatu objek untuk mengambil bentuk atau perilaku yang berbeda dalam konteks yang berbeda. dapat berupa membuat metode dengan nama yang sama tapi dengan parameter yang berbeda maupun mengganti metode yang ada, itu terjadi dalam satu class. jadi pada intinya polimorfisme adalah terdapat beberapa fungsi dalam satu objek. pada kodingan dibawah memang tidak secara langsung menggunakan syntax overriding ataupun overloading, namun pada implementasi dan konsepnya merupakan polimorfisme. 
(mohon maaf pak, saya baru beradaptasi dengan PHP jadi masih agak sulit dalam implementasi syntaxnya, saya coba tapi error pak. hehe)

`<?php

class db_registrasi
{
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "user";
    private $koneksi = "";

    function __construct()
    {
        $this->koneksi = mysqli_connect($this->host, $this->username, $this->password, $this->database);
        if (mysqli_connect_errno()) {
            echo "Koneksi database gagal : " . mysqli_connect_error();
        }
    }

    function getData()
    {
        $hasil = [];
        $data = mysqli_query($this->koneksi, "select * from user");
        while ($row = mysqli_fetch_array($data)) {
            $hasil[] = $row;
        }

        return $hasil;
    }

    function tambah_data($username, $email, $password, $bank, $noRek)
    {
        mysqli_query($this->koneksi, "insert into user (username, email, password, bank, noRek) values ('$username', '$email' , '$password', '$bank', '$noRek' )");
    }

    function get_data_byid($id)
    {
        // $data = mysqli_query($this->koneksi, "select * from user where id='$id'");
        return mysqli_query($this->koneksi, "select * from user where id='$id'")->fetch_array();
    }

    function edit_data($username, $email, $password, $id, $bank, $noRek)
    {
        mysqli_query($this->koneksi, "update user set username='$username', email='$email', password='$password', where id='$id', bank='$bank', noRek ='$noRek'");
    }

    function hapus_data($id)
    {
        mysqli_query($this->koneksi, "delete from user where id='$id'");
    }
}
?>`

sedangkan pada inheritance yang merupakan penurunan class di dalam suatu class, seperti pada contoh : 

`<?php

abstract class Ewallet {
    protected $saldo;

    public function __construct($jumlahSaldo) {
        $this->saldo = $jumlahSaldo;
    }

    abstract public function topUp($jumlah);

    public function getSaldo() {
        return $this->saldo;
    }
}


class GoPay extends Ewallet {
    public function topUp($jumlah) {
        echo "Top-up GoPay: " . $jumlah . PHP_EOL;
        $this->saldo += $jumlah;
    }
}


class OVO extends Ewallet {
    public function topUp($jumlah) {
        echo "Top-up OVO: " . $jumlah . PHP_EOL;
        $this->saldo += $jumlah;
    }
}`

# No 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

![Use Case](https://gitlab.com/adilah-hasan/uts-implementasi-pbo/-/raw/main/USE_CASE_FLIP_ARIH.png)


# No 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

![Use Case](https://gitlab.com/adilah-hasan/uts-implementasi-pbo/-/raw/main/USE_CASE_FLIP_ARIH.png)

![Diagram Class](https://gitlab.com/adilah-hasan/uts-implementasi-pbo/-/raw/main/PBO_DIAGRAM_CLASS.drawio.png)

# No 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

link`https://youtu.be/IIgTmfoM9kM`

# No 10
Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

yang harus diperhatikan ketika kita menentukan UX (User Experience) adalah : 

Inovasi dalam pengalaman pengguna (User Experience atau UX) adalah proses mengembangkan solusi kreatif dan efektif untuk meningkatkan interaksi antara pengguna dan produk atau layanan digital. Inovasi UX melibatkan pemahaman mendalam tentang kebutuhan, preferensi, dan perilaku pengguna, serta penerapan ide-ide baru untuk menciptakan pengalaman yang lebih baik.

Berikut adalah beberapa contoh inovasi UX yang dapat diterapkan:

1. Personalisasi: Membuat pengalaman yang disesuaikan dengan preferensi dan kebutuhan individu pengguna. Ini bisa dilakukan dengan mengumpulkan data pengguna dan menyajikan konten atau fitur yang relevan secara personal.

2. Antarmuka pengguna yang intuitif: Membuat desain antarmuka yang mudah dipahami dan digunakan oleh pengguna. Ini termasuk penggunaan ikon yang jelas, tata letak yang terorganisir dengan baik, dan navigasi yang intuitif.

3. Teknologi yang responsif: Memanfaatkan teknologi seperti AI (Artificial Intelligence) dan ML (Machine Learning) untuk memberikan respons dan rekomendasi yang lebih cepat dan akurat kepada pengguna.

4. Penyederhanaan proses: Mengurangi hambatan dan kompleksitas dalam penggunaan produk atau layanan. Ini dapat dilakukan dengan menghilangkan langkah-langkah yang tidak perlu, memperpendek formulir, atau menyediakan bantuan kontekstual.

5. Pengalaman multichannel: Menyediakan pengalaman yang seragam dan konsisten di berbagai platform dan perangkat, seperti desktop, ponsel, atau tablet.

6. Penggunaan data untuk pengambilan keputusan: Menggunakan data pengguna untuk mengidentifikasi tren dan pola perilaku yang dapat digunakan untuk mengoptimalkan pengalaman pengguna.

7. Integrasi dengan teknologi terkini: Mengintegrasikan teknologi terbaru seperti Augmented Reality (AR), Virtual Reality (VR), atau voice recognition untuk meningkatkan pengalaman pengguna.

8. Kolaborasi dengan pengguna: Melibatkan pengguna dalam proses desain melalui penelitian pengguna, wawancara, dan pengujian prototipe. Ini membantu dalam pemahaman yang lebih baik tentang kebutuhan pengguna dan menghasilkan solusi yang lebih relevan.

9. Design thinking: Menerapkan pendekatan desain berbasis empat tahap, yaitu pemahaman, penentuan masalah, pengembangan solusi, dan pengujian. Pendekatan ini membantu dalam menciptakan solusi yang berfokus pada pengguna.

10. Eksperimen dan iterasi: Mengeksplorasi ide-ide baru dan menguji solusi dengan cepat, kemudian melibatkan pengguna untuk mendapatkan umpan balik dan melakukan iterasi berkelanjutan untuk meningkatkan pengalaman pengguna.

![Contoh Inovasi UX](https://gitlab.com/adilah-hasan/uts-implementasi-pbo/-/raw/main/flip.png)
